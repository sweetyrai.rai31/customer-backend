const express = require('express')
const {addCustomer, getAllCustomers, getCustomer, updateCustomer, deleteCustomer} = require('../controllers/customerController')

const router = express.Router()

router.post('/add-customer', addCustomer)
router.get('/get-customers', getAllCustomers)
router.get('/customer/:id', getCustomer)
router.put('/update-customer/:id', updateCustomer)
router.post('/delete-customer', deleteCustomer)

module.exports = {
    routes : router
}