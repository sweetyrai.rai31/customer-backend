class Customer {
    constructor(id, name, email, customer_file){
        this.id = id
        this.name = name
        this.email = email
        this.customer_file = customer_file
    }
}

module.exports = Customer