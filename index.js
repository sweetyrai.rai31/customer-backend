"use strict"

//variable added
const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const config = require("./config")
const customerRoutes = require('./routes/customer-routes')

const app = express()
const path = require('path')


app.use(express.json())
app.use(cors())
app.use(bodyParser.json())

app.use('/api', customerRoutes.routes)

// Serve static files from the React frontend app
app.use(express.static(path.join(__dirname, './frontend')))

// AFTER defining routes: Anything that doesn't match what's above, send back index.html; (the beginning slash ('/') in the string is important!)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, './frontend', '/index.html')) 
})

app.listen(config.port, ()=> console.log("App is listening on: "+config.port))