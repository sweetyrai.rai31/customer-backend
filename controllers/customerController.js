const firebase = require('../db')
const Customer = require('../models/customer')
const multer = require('multer')
const firestore = firebase.firestore()
// const admin = require('firebase-admin')

// Cloud storage
// const bucket = firebase.storage().bucket()

const addCustomer = async(req, res, next) => {
    try {
        const data = req.body

        await firestore.collection('customer').doc().set(data)
            
            res.send({status: 200,
                message: 'Record saved successfully'

            })
    } catch (error) {
        // res.status(400).send(error.message)
        res.send({status: 201,
            message: 'Failed to save customer record'

        })
    }    
}

const getAllCustomers = async(req, res, next) => {
    try {
        
        const customer =  await firestore.collection('customer')
        const data = await customer.get()
        const customerArray = []
        if(data.empty) {
            // res.status(404).send('No customer record found')
            res.send({
                status:201,
                message: 'No customer record found'
            })
        }
        else {
            data.forEach(doc => {
                const cust = new Customer(
                    doc.id,
                    doc.data().name,
                    doc.data().email,
                    doc.data().customer_file
                )
                customerArray.push(cust)
            });

            res.send({
                status: 200,
                data: customerArray
            })
        }
    } catch (error) {
        res.send({
            status: 201,
            message: error.message
        })
        // res.status(400).send(error.message)
    }    
}


const getCustomer = async(req, res, next) => {
    try {
        const id = req.params.id
        const customer = await firestore.collection("customer").doc(id)
        const data = await customer.get()
        if(!data.exists) {
            res.status(404).send("Customer with the given ID not found")
        }
        else {
            
            res.send({
                status: 200,
                data:data.data() 
            })
        }
    } catch (error) {
        res.status(400).send(error.message)       
    }
}

const updateCustomer = async (req, res, next)=> {

    try {
        const id = req.params.id
        const data = req.body
        const customer = await firestore.collection("customer").doc(id)
        await customer.update(data)
        res.send({
            status : 200,
            message: "Customer record updated successfully"
        })
    } catch (error) {
        res.status(400).send(error.message)
    }
}

const deleteCustomer = async (req, res, next)=> {
    try {
        const id = req.body.id
        const data = req.body
        const customer = await firestore.collection("customer").doc(id)
        await customer.delete(data)
        res.send({
            status : 200,
            message: "Customer record deleted successfully"
        })
    } catch (error) {
        res.status(400).send(error.message)
    }
}

module.exports = {
    addCustomer,
    getAllCustomers,
    getCustomer,
    updateCustomer,
    deleteCustomer
}