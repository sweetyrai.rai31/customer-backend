  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCbk0GAvcac3xChvi3Kp9y_kTDYZoxsmKs",
    authDomain: "customers-15cd8.firebaseapp.com",
    databaseURL: "https://customers-15cd8-default-rtdb.firebaseio.com",
    projectId: "customers-15cd8",
    storageBucket: "customers-15cd8.appspot.com",
    messagingSenderId: "643384495888",
    appId: "1:643384495888:web:1dd78292b08928077b1f95",
    measurementId: "G-BYQQPE249C"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

document.getElementById("fileUpload").onchange = function (event) {
    const files = event.target.files
    const formData = new FormData()
    formData.append('myFile', files[0])
    document.getElementById('file-name').innerHTML = files[0].name
  }


  function deleteRecords(customer) {
    localStorage.setItem("customer_id", customer.id);
    var data = {
        id: customer.id,
        name: customer.name,
        email:customer.email
    }
    fetch('https://customer-test-app.herokuapp.com/api/delete-customer', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
        if(data.status == 200) {
            location.reload();
            getCustomers()
        }
      console.log(data.message)
    })
    .catch(error => {
      console.error(error)
    })  
  }

  const editRecords = customer => {
    
    localStorage.setItem("customer_id", customer.id);
      document.getElementById('submit').style.display = "none";
      document.getElementById('update').style.display = "block"; 
      document.getElementById('name').value = customer.name
      document.getElementById('email').value = customer.email
      
  }
  
  
  document.getElementById("attachBtn").onclick = function () { 
    document.getElementById('fileUpload').click(); 
   };
   document.getElementById("submit").onclick = function () { 
     
    var alphaExp = /^[A-Za-z]+$/
    var email_regx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var name = document.getElementById("name").value
    var email = document.getElementById("email").value
    if(alphaExp.test(name) == false) {
      document.getElementById("name_error").innerHTML = 'Invalid character'
      document.getElementById("name_error").style.visibility = "visible";
    }
    if (email_regx.test(email) == false) 
    {
      document.getElementById("email_error").innerHTML = 'Invalid Email Address'
      document.getElementById("email_error").style.visibility = "visible"; 
    }
    if(name == '') {
      document.getElementById("name_error").innerHTML = 'Name is required'
      document.getElementById("name_error").style.visibility = "visible"; 
    }
    if(email == '') {
      document.getElementById("email_error").innerHTML = 'Email id is required'
      document.getElementById("email_error").style.visibility = "visible"; 
    }
     console.log('file->', document.getElementById('fileUpload').files[0]);
    if(document.getElementById('fileUpload').files[0] != undefined) {
      const customer_file_field = document.getElementById('fileUpload').files[0];
     const ref = firebase.storage().ref()
     var file_name = new Date() + '-' + customer_file_field
     var metadata = {
       contentType : customer_file_field.type
     }
     const task = ref.child(file_name).put(customer_file_field, metadata)
     task.then(snapshot => snapshot.ref.getDownloadURL())
     .then(url => {
       console.log('url->', url);    
    
    if(name != '' && email != '' && email_regx.test(email) == true && alphaExp.test(name) == true) {
      
        var data = {
          name: name,
          email:email,
          customer_file: url
        }
        
        fetch('https://customer-test-app.herokuapp.com/api/add-customer', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
            if(data.status == 200) {
              location.reload();
                document.querySelector("#name").value = ""
                document.querySelector("#email").value = ""
                getCustomers()
            }
          console.log(data.message)
        })
        .catch(error => {
          console.error(error)
        })
      }
     })
    }
    else {
      if(name != '' && email != '' && email_regx.test(email) == true && alphaExp.test(name) == true) {
      
        var data = {
          name: name,
          email:email,
          customer_file: ''
        }
        
        fetch('https://customer-test-app.herokuapp.com/api/add-customer', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
            if(data.status == 200) {
              location.reload();
                document.querySelector("#name").value = ""
                document.querySelector("#email").value = ""
                getCustomers()
            }
          console.log(data.message)
        })
        .catch(error => {
          console.error(error)
        })
      }
    }



     
     

    
   };
   function getCustomers() {
    document.getElementById('loader').style.display = "block"; 
    document.getElementById('no_records').style.display = "none"; 
       fetch('https://customer-test-app.herokuapp.com/api/get-customers', {
        method: 'GET',
      })
      .then(response => response.json())
      .then(data => {
          var customer_arr = data.data
          if(data.status == 200) {
            document.getElementById('loader').style.display = "none"; 
            customer_arr.forEach((customer,i)=> {
              console.log('i->', customer.customer_file != '');
              console.log('file->', customer.customer_file);
                var tr = document.createElement("tr");
                var td1 = document.createElement("td")
                td1.setAttribute("data-label", "Name");
                td1.innerHTML = customer.name;
                var td2 = document.createElement("td")
                td2.setAttribute("data-label", "Email");
                td2.innerHTML = customer.email;
                var td3 = document.createElement("td")
                td3.setAttribute("data-label", "File");
                if(customer.customer_file != '') {
                  td3.innerHTML = '<a id="file'+i+'" target="_blank" href='+customer.customer_file+'>file</a>'
                }
                else {
                  td3.innerHTML = 'file'
                }
                var td4 = document.createElement("td")
                td4.setAttribute("data-label", "Action");
                td4.innerHTML = '<a class="edit" id="edit'+i+'" href="#">Edit</a>  <a class="delete" id="delete'+i+'" href="#">Delete</a>'
                tr.appendChild(td1); 
                tr.appendChild(td2); 
                tr.appendChild(td3); 
                tr.appendChild(td4); 
                document.getElementById('table-data').appendChild(tr);  
                document.querySelector('#edit'+ i).addEventListener('click', event => {
                    editRecords(customer)
                })
                document.querySelector('#delete'+ i).addEventListener('click', event => {
                    deleteRecords(customer)
                })
                if(customer.customer_file != '') {
                  document.querySelector('#file'+ i).addEventListener('click', event => {
                    downloadFile(customer)
                  })
                }

                
            })
            
          }
          else {
            document.getElementById('loader').style.display = "none"; 
            document.getElementById('no_records').style.display = "block"; 
          }
        console.log(data.message)
      })
      .catch(error => {
        console.error(error)
      })
   }
  function updateRecords() {
    var name = document.querySelector("#name").value
    var email = document.querySelector("#email").value
    if(name == '') {
      document.getElementById("name_error").innerHTML = 'Name is required'
      document.getElementById("name_error").style.visibility = "visible"; 
    }
    if(email == '') {
      document.getElementById("email_error").innerHTML = 'Email id is required'
      document.getElementById("email_error").style.visibility = "visible"; 
    }
      var data = {
          name : name,
          email : email
      }
    fetch('https://customer-test-app.herokuapp.com/api/update-customer/'+ localStorage.getItem("customer_id"), {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
        if(data.status == 200) {
            location.reload();
            getCustomers()
        }
      console.log(data.message)
    })
    .catch(error => {
      console.error(error)
    })   
  }